# Resale Shop

This application allows an employee to add, update and remove 
items from the store. Employees are also allowed to view all the
purchased items. Customers are allowed to view items but not purchase 
without an account.

## Getting Started

The username for an employee is 'employee', the password is 'employeePass', and the role is '1'. The 
token that must be used for authentication is 'employee-auth-token', it is also
generated when logging in.

The username for a customer is 'customer', the password is 'customerPass', and the role is '2'. The
token that must be used for authentication is 'customer-auth-token', it is also
generated when logging in.

## ER Diagram

![img.png](img.png)
package com.crosley.beans;



import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Users implements Serializable {
    private int userRole;//1 for employee, 2 for cust
    private int userId;
    private String username;
    private String userpass;
    private String firstname;
    private String lastname;
    private String address;
    private String email;

    public Users() {
    }
//constructor for user employee
    public Users(int userId, int userRole,  String username, String userpass, String firstname, String lastname, String address, String email) {
        this.userRole = userRole;
        this.userId = userId;
        this.username = username;
        this.userpass = userpass;
        this.firstname = firstname;
        this.lastname = lastname;
        this.address = address;
        this.email = email;
    }

    public int getUserRole() { return userRole; }

    public void setUserRole(int userRole) { this.userRole = userRole; }

    public int getUserId() { return userId; }

    public void setUserId(int userId) { this.userId = userId; }

    public String getUsername() { return username; }

    public void setUsername(String username) { this.username = username; }

    public String getUserpass() { return userpass; }

    public void setUserpass(String userpass) { this.userpass = userpass; }

    public String getFirstname() { return firstname; }

    public void setFirstname(String firstname) { this.firstname = firstname; }

    public String getLastname() { return lastname; }

    public void setLastname(String lastname) { this.lastname = lastname; }

    public String getAddress() { return address; }

    public void setAddress(String address) { this.address = address; }

    public String getEmail() { return email; }

    public void setEmail(String email) { this.email = email; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Users users = (Users) o;
        return userRole == users.userRole && userId == users.userId && Objects.equals(username, users.username) && Objects.equals(userpass, users.userpass) && Objects.equals(firstname, users.firstname) && Objects.equals(lastname, users.lastname) && Objects.equals(address, users.address) && Objects.equals(email, users.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userRole, userId, username, userpass, firstname, lastname, address, email);
    }

    @Override
    public String toString() {
        return "Users{" +
                "userRole=" + userRole +
                ", userId=" + userId +
                ", username='" + username + '\'' +
                ", userpass='" + userpass + '\'' +
                ", firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", address='" + address + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}

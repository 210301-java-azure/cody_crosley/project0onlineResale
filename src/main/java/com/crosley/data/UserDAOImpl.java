package com.crosley.data;

import com.crosley.beans.Users;
import com.crosley.util.ConnectionUtilitie;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;

public class UserDAOImpl implements UserDAO{


    private Logger log = LoggerFactory.getLogger(UserDAOImpl.class);
    String connectionMade = "connection made";
    @Override
    public List<Users> getAllUsers() {

        log.info("attempting to make connection to retrieve all users");
        try(Connection con = ConnectionUtilitie.getConnection();
            Statement st = con.createStatement();) {
            log.info(connectionMade);
            ResultSet rs = st.executeQuery("select * from users");
            List<Users> usersList = new ArrayList<>();
            while (rs.next()){
                int userId = rs.getInt("user_id");
                int userRole = rs.getInt("user_role");
                String username = rs.getString("username");
                String userpass = rs.getString("userpass");
                String firstname = rs.getString("firstname");
                String lastname = rs.getString("lastname");
                String address = rs.getString("address");
                String email = rs.getString("email");
                Users user = new  Users(userId, userRole, username, userpass, firstname, lastname, address, email);
                usersList.add(user);
            }
            return usersList;
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException classNotFoundException) {
            classNotFoundException.printStackTrace();
        }
        return null;
    }

    @Override
    public Users getUserById(int id) {
        log.info("attempting to make connection to retrieve user " + id);
        try(Connection con = ConnectionUtilitie.getConnection();
            PreparedStatement pdst = con.prepareStatement("select * from users where user_id = ?")) {
            log.info(connectionMade);
            Users users = new Users();
            pdst.setInt(1, id);
            ResultSet rs = pdst.executeQuery();
            if(rs.next()){
                int userId = rs.getInt("user_id");
                int userRole = rs.getInt("user_role");
                String username = rs.getString("username");
                String userpass = rs.getString("userpass");
                String firstname = rs.getString("firstname");
                String lastname = rs.getString("lastname");
                String address = rs.getString("address");
                String email = rs.getString("email");
                Users user = new  Users(userId, userRole, username, userpass, firstname, lastname, address, email);
                log.info("user " + id + " retrieved");
                return user;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException classNotFoundException) {
            classNotFoundException.printStackTrace();
        }
        return null;
    }

    @Override
    public Users addNewUser(Users users) {
        log.info("attempting to make connection to add a user");
        try(Connection con = ConnectionUtilitie.getConnection();
            PreparedStatement pdst = con.prepareStatement("insert into users values(default, ?, ?, ?, ?, ?, ?, ?)")) {
            log.info(connectionMade);
            pdst.setInt(1, users.getUserRole());
            pdst.setString(2, users.getUsername());
            pdst.setString(3, users.getUserpass());
            pdst.setString(4, users.getFirstname());
            pdst.setString(5, users.getLastname());
            pdst.setString(6, users.getAddress());
            pdst.setString(7, users.getEmail());
            pdst.executeUpdate();
            log.info("user added");

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException classNotFoundException) {
            classNotFoundException.printStackTrace();
        }

        return users;
    }

    @Override
    public Users updateUser(Users users) {
        log.info("attempting to make connection to update a user");
        try(Connection con = ConnectionUtilitie.getConnection();
            PreparedStatement pdst = con.prepareStatement("update users set username = ?,userpass = ?, "
                    + " firstname = ?, lastname = ?, address = ?, email = ?"
                    + " where user_id = " + users.getUserId())) {
            log.info("connection made");
            pdst.setString(1, users.getUsername());
            pdst.setString(2, users.getUserpass());
            pdst.setString(3, users.getFirstname());
            pdst.setString(4, users.getLastname());
            pdst.setString(5, users.getAddress());
            pdst.setString(6, users.getEmail());
            pdst.executeUpdate();

            log.info("user updated");
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException classNotFoundException) {
            classNotFoundException.printStackTrace();
        }
        return null;
    }

    @Override
    public void removeUser(int id )  {
        log.info("attempting to make connection to remove user");
        try(Connection con = ConnectionUtilitie.getConnection();
            PreparedStatement pdst = con.prepareStatement("delete from users where user_id = ?")){
            log.info(connectionMade);

            pdst.setInt(1, id);
            if(pdst.executeUpdate() == 1){
                log.info("item " + id + " removed");

            }else {
                log.info("delete failed");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException classNotFoundException) {
            classNotFoundException.printStackTrace();
        }
    }
}

package com.crosley;

import com.crosley.controllers.*;
import io.javalin.Javalin;
import static io.javalin.apibuilder.ApiBuilder.*;


public class ResaleStoreApp {
    public static void main(String[] args) {

        Javalin app = Javalin.create().start(7000);

        ItemController itemController = new ItemController();
        UserController userController = new UserController();
        PurchasesController purchasesController = new PurchasesController();
        AuthController authController = new AuthController();

        String update = "/update";
        String remove = "/remove";

//handlers
        app.routes(() -> {
//items handlers
            before("/items/addItem",authController::employeeAuthToken);
            before("/items/:id/update",authController::employeeAuthToken);
            before("/items/:id/remove",authController::employeeAuthToken);
            path("/items", () -> {
                get(itemController::handleGetAllItems);
                path("/addItem", () -> {
                    post(itemController::handlePostNewItem);
                });
                    path("/:itemId", () -> {
                    get(itemController::handleGetItemById);

                    path(update, () -> {
                        patch(itemController::handlePostUpdateItem);
                    });
                    path(remove, () -> {
                        post(itemController::handlePostRemoveItemById);
                    });
                });
            });
//users handlers
            before("/users",authController::employeeAuthToken);
            path("/users", () -> {
                get(userController::handleGetAllUsers);
                post(userController::handlePostNewUser);
                path("/:userId", () -> {
                    get(userController::handleGetUserById);
                    path(update, () -> {
                        patch(userController::handleUpdateUser);
                    });
                    path(remove, () -> {
                        post(userController::handleRemoveUser);
                    });
                });
            });
//purchased handlers
            before("/purchased/update",authController::custAuthToken);
            path("/purchased", () -> {
                get(purchasesController::handleGetAllPurchases);
                path("/:id", () -> {
                    get(purchasesController::handleGetPurchase);
                    path(remove, () -> {
                        post(purchasesController::handlePostRemovePurchase);
                    });
                });
                path(update, () -> {
                    post(purchasesController::handlePostNewPurchase);
                });

            });
//login handlers
            path("/login", () -> {
                post(authController::employeeAuthLogin);
                path("/cust", () -> {
                    post(authController::custAuthLogin);
                });
            });

        });
    }
}


package com.crosley.services;

import com.crosley.beans.Items;
import com.crosley.data.ItemDAOImpl;

import java.util.List;

public class ItemService {
    private ItemDAOImpl itemDaoImpl = new ItemDAOImpl();

    public List<Items> getAll(){
        return itemDaoImpl.getAllItems();
    }
    public Items getItem(int id) {return  itemDaoImpl.getItemById(id);}
    public Items addItem(Items items) { return itemDaoImpl.addNewItem(items);}
    public void removeItem(int id) {itemDaoImpl.removeItem(id); }
    public Items updateItem(Items itemId) { return itemDaoImpl.updateItem(itemId);}
}
